<?php

use Illuminate\Http\Request;
use App\Empleado;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('datos', function () {
    return Empleado::all();
});

Route::post('datos', function (Request $request) {
    //Validando los $request
    $request->validate([
        'nombres'           =>  'required|string|max:50',
        'apellidos'         =>  'required|string|max:50',
        'cedula'            =>  'required|max:12',
        'email'             =>  'required|email|unique:empleados',
        'lugar_nacimiento'  =>  'required|max:50',
        'sexo'              =>  'required|max:50',
        'estado_civil'      =>  'required|max:50',
        'telefono'          =>  'required|max:20',
    ]);
    $empleado = new Empleado();
    //Seteando valores
    $empleado->nombres=$request->nombres;
    $empleado->apellidos=$request->apellidos;
    $empleado->cedula=$request->cedula;
    $empleado->email=$request->email;
    $empleado->lugar_nacimiento=$request->lugar_nacimiento;
    $empleado->sexo=$request->sexo;
    $empleado->estado_civil=$request->estado_civil;
    $empleado->telefono=$request->telefono;

    $empleado->save();
    return $request;
});

Route::put('datos/{id}', function (Request $request, $id) {
    try{
        $empleado = Empleado::find($id);

        $empleado->nombres=$request->nombres;
        $empleado->apellidos=$request->apellidos;
        $empleado->cedula=$request->cedula;
        $empleado->email=$request->email;
        $empleado->lugar_nacimiento=$request->lugar_nacimiento;
        $empleado->sexo=$request->sexo;
        $empleado->estado_civil=$request->estado_civil;
        $empleado->telefono=$request->telefono;

        $empleado->save();
        return 'Exito!';
    }
    catch(Exception $e){
        return 'Error '.$e->getMessage();
    }
    

});

Route::delete('datos/{id}', function (Request $request, $id) {
    try{
        $empleado = Empleado::findOrFail($id);
        $empleado->delete();
        return 'Exito al borrar!';
    }
    catch(Exception $e){
        return 'Error '.$e->getMessage();
    }
    

});