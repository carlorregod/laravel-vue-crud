<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('prueba', function () {
    return 'Prueba 1 con get';
});

Route::post('prueba', function () {
    return 'Prueba 2 con post';
});

Route::put('prueba/{id}', function ($id) {
    return 'Prueba 3 con put '.$id;
});

